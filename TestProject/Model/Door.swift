//
//  Door.swift
//  TestProject
//
//  Created by Виктор Сирик on 15.04.2022.
//

import Foundation

enum StatusCell {
    case locked
    case load
    case unlocked
}

struct Door {
    var type: String?
    var isLocked = true
    var statusDoor: StatusCell = .locked
}
