//
//  DoorRealm.swift
//  TestProject
//
//  Created by Виктор Сирик on 16.04.2022.
//

import Foundation
import RealmSwift

 enum StatusCellDatabase: String, PersistableEnum {
    case locked
    case load
    case unlocked
}

class DoorsList: Object {
    @Persisted var listDoors: List<DoorDatabase> = List<DoorDatabase>()
}

class DoorDatabase: Object{
    @Persisted var type: String?
    @Persisted var isLocked: Bool?
    @Persisted var statusDoor: StatusCellDatabase = .locked
}
