//
//  DoorCell.swift
//  TestProject
//
//  Created by Виктор Сирик on 15.04.2022.
//

import UIKit
import SnapKit

protocol DoorCellDelegate {
    func lockedDoorAction(index: Int, statusType: StatusCell, isAnimated: Bool)
}

class DoorCell: UITableViewCell, CAAnimationDelegate {
    var isSecureImage = UIImageView()
    var isLockedImage = UIImageView()
    var frontDoorLabel = UILabel()
    var typeLabel = UILabel()
    var isLockedButton = UIButton()
    var delegate: DoorCellDelegate?
    let shapeLayer = CAShapeLayer()
    var doorData: Door?

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }

    private func setup() {
        contentView.addSubview(isSecureImage)
        contentView.addSubview(isLockedImage)
        contentView.addSubview(frontDoorLabel)
        contentView.addSubview(typeLabel)
        contentView.addSubview(isLockedButton)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        isSecureImage.snp.makeConstraints{ maker in
            maker.top.equalTo(contentView).offset(18)
            maker.left.equalTo(contentView).offset(27)
            maker.width.equalTo(41)
            maker.height.equalTo(41)
        }

        isLockedImage.snp.makeConstraints{ maker in
            maker.top.equalTo(contentView).offset(18)
            maker.right.equalTo(contentView).offset(-22)
            maker.width.equalTo(40)
            maker.height.equalTo(45)
        }
        
        frontDoorLabel.snp.makeConstraints{ maker in
            maker.top.equalTo(contentView).offset(22)
            maker.left.equalTo(isSecureImage.snp.right).inset(-14)
        }
        
        typeLabel.snp.makeConstraints{ maker in
            maker.top.equalTo(frontDoorLabel.snp.bottom).inset(-1)
            maker.left.equalTo(isSecureImage.snp.right).inset(-14)
        }
        
        isLockedButton.snp.makeConstraints{ maker in
            maker.bottom.equalTo(contentView).inset(10)
            maker.centerX.equalTo(contentView)
        }
    }
    
    @objc func isLockedAction() {
        delegate?.lockedDoorAction(index: tag, statusType: doorData?.statusDoor ?? .locked, isAnimated: true)
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.shapeLayer.removeFromSuperlayer()
        self.delegate?.lockedDoorAction(index: self.tag, statusType: self.doorData?.statusDoor ?? .locked, isAnimated: false )
    }
    
    func update(door: Door) {
        switch door.statusDoor{
        case.locked:
            isLockedImage.isHidden = false
            isSecureImage.image = UIImage(named: "secure")
            isLockedButton.setTitle("Locked", for: .normal)
            isLockedButton.setTitleColor(UIColor(red:0/255.0, green:68/255.0, blue:139/255.0, alpha:1.0), for: .normal)
            isLockedImage.image = UIImage(named: "locked")

        case .unlocked:
            isLockedImage.isHidden = false
            isSecureImage.image = UIImage(named: "unsecure")
            isLockedButton.setTitle("Unlocked", for: .normal)
            isLockedButton.setTitleColor(UIColor(red:0/255.0, green:68/255.0, blue:139/255.0, alpha:0.5), for: .normal)
            isLockedImage.image = UIImage(named: "unlocked")

        case .load:
            if !door.isLocked {
                isLockedButton.setTitle("Unlocking...", for: .normal)
            } else { isLockedButton.setTitle("Locking...", for: .normal) }

            isSecureImage.image = UIImage(named: "unsecuring")
            isLockedButton.setTitleColor(UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha:0.17), for: .normal)
            isLockedImage.isHidden = true
            addCircleProgressView()

            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.toValue = 1
            animation.duration = 3
            animation.fillMode = .forwards
            animation.delegate = self
            self.shapeLayer.add(animation, forKey: "animation")
        }
        typeLabel.text = door.type
        doorData = door
        
        frontDoorLabel.text = "Front door"
        frontDoorLabel.font = UIFont(name: "Sk-Modernist-Bold", size: 16)
        frontDoorLabel.frame.size = CGSize(width: 77, height: 19)
        
        typeLabel.font = UIFont(name: "Sk-Modernist-Regular", size: 14)
        typeLabel.frame.size = CGSize(width: 77, height: 19)
        
        isLockedButton.titleLabel?.font = UIFont(name: "Sk-Modernist-Bold", size: 14)
        isLockedButton.frame.size = CGSize(width: 52, height: 17)
        isLockedButton.addTarget(self, action: #selector(isLockedAction), for: .touchUpInside)

    }
    
    func addCircleProgressView() {
        let center = isLockedImage.center
        let circularPath = UIBezierPath(arcCenter: center, radius: 15, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: true)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor(red:43/255.0, green:184/255.0, blue:79/255.0, alpha:1).cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.strokeEnd = 0
        contentView.layer.addSublayer(shapeLayer)
    }
}

