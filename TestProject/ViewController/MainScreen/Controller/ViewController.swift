//
//  ViewController.swift
//  TestProject
//
//  Created by Виктор Сирик on 14.04.2022.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    let settingButton = UIButton()
    let passwordButton = UIButton()
    let welcomeLabel = UILabel()
    let doorsTableView = UITableView()
    let mainHomeImage = UIImageView()
    let headerView = UIView()
    let interLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 49, height: 17))
    var doors: [Door] = []
    var isDoorUnlocked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        doors = FillData().fillArray()
        createMainLabel()
        createGradient()
        createHomeImage()
        setTableView()
        createButtonSettings()
    }
    
    func createMainLabel() {
        interLabel.text = "Inter"
        interLabel.font = UIFont(name: "Sk-Modernist-Bold", size: 25)
        let color = UIColor(red:167/255.0, green:168.0/255.0, blue:170.0/255.0, alpha:1.0)
        interLabel.textColor = color
        view.addSubview(interLabel)
        interLabel.snp.makeConstraints{ maker in
            maker.top.equalToSuperview().inset(77)
            maker.left.equalToSuperview().inset(24)
        }
        
        welcomeLabel.text = "Welcome"
        welcomeLabel.font = UIFont(name: "Sk-Modernist-Bold", size: 35)
        welcomeLabel.textColor = UIColor.black
        welcomeLabel.frame.size = CGSize(width: 148, height: 92)
        view.addSubview(welcomeLabel)
        welcomeLabel.snp.makeConstraints{ maker in
            maker.left.equalToSuperview().inset(24)
            maker.top.equalTo(interLabel.snp.bottom).inset(-50)
        }
    }
    
    func createGradient()  {
        let gradientView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor(red:0/255.0, green:68/255.0, blue:139/255.0, alpha:1.0).cgColor, UIColor(red:0/255.0, green:143.0/255.0, blue:211/255.0, alpha:1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = gradientView.bounds
        gradientView.layer.addSublayer(gradient)
        self.view.addSubview(gradientView)
        gradientView.snp.makeConstraints{ maker in
            maker.top.equalTo(interLabel.snp.top)
            maker.left.equalTo(interLabel.snp.right)
        }
       
        let qrLabel = UILabel(frame: gradientView.frame)
        qrLabel.text = "QR"
        qrLabel.font = UIFont(name: "Sk-Modernist-Bold", size: 25)
        qrLabel.textAlignment = .center
        gradientView.addSubview(qrLabel)
        gradientView.mask = qrLabel
        
        qrLabel.snp.makeConstraints{ maker in
            maker.top.equalTo(interLabel.snp.top)
            maker.left.equalTo(interLabel.snp.right).inset(0)
        }
    }
    
    func createButtonSettings() {
        settingButton.layer.borderWidth = 1
        settingButton.layer.cornerRadius = 13
        settingButton.setImage(UIImage(systemName: "gearshape"), for: .normal)
        settingButton.tintColor = UIColor(red:32/255.0, green:14/255.0, blue:50/255.0, alpha:1.0)
        settingButton.layer.borderColor = UIColor(red:225/255.0, green:232/255.0, blue:232/255.0, alpha:1.0).cgColor
        view.addSubview(settingButton)
        settingButton.snp.makeConstraints{ maker in
            maker.top.equalToSuperview().inset(63)
            maker.width.equalTo(45)
            maker.height.equalTo(45)
            maker.right.equalToSuperview().inset(27)
        }
        passwordButton.setImage(UIImage(named: "Password"), for: .normal)
        view.addSubview(passwordButton)
        passwordButton.snp.makeConstraints{ maker in
            maker.width.equalTo(18.5)
            maker.height.equalTo(18.5)
            maker.right.equalToSuperview().inset(48)
            maker.bottom.equalToSuperview().inset(14)
        }
    }
    
    func createHomeImage() {
        mainHomeImage.image = UIImage(named: "Home")
        view.addSubview(mainHomeImage)
        mainHomeImage.snp.makeConstraints { maker in
            maker.top.equalToSuperview().inset(108)
            maker.left.equalTo(welcomeLabel.snp.right).inset(-23)
        }
    }
    
    func setTableView() {
        doorsTableView.dataSource = self
        doorsTableView.delegate = self
        doorsTableView.reloadData()
        view.addSubview(doorsTableView)

        doorsTableView.snp.makeConstraints { maker in
            maker.top.equalTo(mainHomeImage.snp.bottom).inset(-84)
            maker.left.equalToSuperview().inset(10)
            maker.right.equalToSuperview().inset(10)
            maker.bottom.equalToSuperview().inset(0)
        }
        doorsTableView.register(DoorCell.self, forCellReuseIdentifier: "DoorCell")
    }
}

