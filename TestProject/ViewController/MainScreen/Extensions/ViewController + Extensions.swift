//
//  ViewController + Extensions.swift
//  TestProject
//
//  Created by Виктор Сирик on 15.04.2022.
//

import Foundation
import UIKit

extension ViewController: UITableViewDataSource, UITableViewDelegate, DoorCellDelegate{
    func lockedDoorAction(index: Int, statusType: StatusCell, isAnimated: Bool) {
        if !isDoorUnlocked || !isAnimated{
            switch statusType {
            case .locked:
                doors[index].statusDoor = .load
                doors[index].isLocked = false
            case .load:
                if doors[index].isLocked {doors[index].statusDoor = .locked
                } else { doors[index].statusDoor = .unlocked }
            case .unlocked:
                doors[index].statusDoor = .load
                doors[index].isLocked = true
            }
            isDoorUnlocked = !isDoorUnlocked
            doorsTableView.reloadData()
        } else {
            for item in doors {
                if item.statusDoor == .load {
                    let alert = AlertManager().setAlert(isLocked: doors[index].isLocked)
                    present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        DatabaseManager.shared.saveData(data: doors)
        return doors.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 117
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoorCell", for: indexPath) as! DoorCell
        cell.selectionStyle = .none
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor(red:227/255.0, green:234/255.0, blue:234/255.0, alpha:1).cgColor
        cell.layer.cornerRadius = 15
        cell.tag = indexPath.section
        cell.update(door: doors[indexPath.section])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section{
        case 0:
            let headerView = UIView(frame: CGRect(x: 25, y: -29, width: doorsTableView.frame.width, height: 40))
            let label = UILabel(frame: headerView.frame)
            label.text = "My doors"
            label.textColor = .black
            label.font = UIFont(name: "Sk-Modernist-Bold", size: 20)
            label.textAlignment = .left
            headerView.addSubview(label)
            return headerView
        default:
            let spacingView = UIView()
            spacingView.backgroundColor = UIColor.clear
            return spacingView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0: return 40
        default: return 10
        }
    }
}
