//
//  LaunchViewController.swift
//  TestProject
//
//  Created by Виктор Сирик on 16.04.2022.
//

import UIKit

class LoadViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.systemBlue
        addActivityIndicator()
    }
    
    func addActivityIndicator() {
        let ai = UIActivityIndicatorView(style: .large)
        ai.center = view.center
        view.addSubview(ai)
        ai.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            ai.stopAnimating()
            ai.isHidden = true
            let mainScreen = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(mainScreen, animated: true)

        }
    }
}
