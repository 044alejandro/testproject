//
//  AlertManager.swift
//  TestProject
//
//  Created by Виктор Сирик on 17.04.2022.
//

import Foundation
import UIKit

class AlertManager {
    func setAlert(isLocked: Bool) -> UIAlertController {
        let locked = isLocked ? "locking" : "unlocking"
        let alert = UIAlertController(title: "Attention!", message: "Now \(locked) another door", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .cancel)
        alert.addAction(cancel)
        return alert
    }
}
