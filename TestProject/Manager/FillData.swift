//
//  FillData.swift
//  TestProject
//
//  Created by Виктор Сирик on 16.04.2022.
//

import Foundation

class FillData {
    func fillArray() -> [Door] {
        guard let doorsDB = DatabaseManager.shared.getData() else {
            let doors = [Door(type: "Home"), Door(type: "Office"),Door(type: "Home"),Door(type: "Office"),Door(type: "Office"),Door(type: "Home")]
            return doors
        }
        return doorsDB
    }
}
