//
//  DatabaseManager.swift
//  TestProject
//
//  Created by Виктор Сирик on 16.04.2022.
//

import Foundation
import RealmSwift

class DatabaseManager{
    
    static let shared = DatabaseManager()
    private let realm = try? Realm()
    
    func saveData(data: [Door]) {
//        let defaultPath = Realm.Configuration.defaultConfiguration.fileURL?.path
//                try! FileManager.default.removeItem(atPath: defaultPath!)
        let transformToDb = transformToDatabase(data: data)
        do{
            try realm?.write({
               realm?.add(transformToDb)
            })
        } catch (let error){
            print(error.localizedDescription)
        }
    }
    
    func transformToDatabase(data: [Door]) -> DoorsList {
        let doorsDatabase: List<DoorDatabase> = List<DoorDatabase>()
        let listDoors = DoorsList()
        for item in data {
            let door = DoorDatabase()
            door.type = item.type
            door.isLocked = item.isLocked
            switch item.statusDoor{
            case .locked: door.statusDoor = .locked
            case .unlocked: door.statusDoor = .unlocked
            case .load: door.statusDoor = .locked
            }
            doorsDatabase.append(door)
        }
        listDoors.listDoors = doorsDatabase
        return listDoors
    }

    func transformToDataModel(dataDatabase: DoorsList) -> [Door] {
        var doorModel: [Door] = []
        for item in dataDatabase.listDoors {
            var door = Door(type: "")
            door.type = item.type
            door.isLocked = item.isLocked ?? true
            switch item.statusDoor{
            case .locked: door.statusDoor = .locked
            case .unlocked: door.statusDoor = .unlocked
            case .load: door.statusDoor = .locked
            }
            doorModel.append(door)
        }
        return doorModel
    }
    
    func getData() -> [Door]? {
        guard let doorsDatabase = realm?.objects(DoorsList.self).last else { return nil }
        return transformToDataModel(dataDatabase: doorsDatabase)
    }
}
